package Relatorios;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import com.aspose.words.Cell;
import com.aspose.words.Document;
import com.aspose.words.DocumentBuilder;
import com.aspose.words.FindReplaceOptions;
import com.aspose.words.Node;
import com.aspose.words.NodeType;
import com.aspose.words.Paragraph;
import com.aspose.words.Row;
import com.aspose.words.Run;
import com.aspose.words.Table;
import DTOs.GestaoRiscosDTO;
import DTOs.GestaoRiscosTeste;
import Utilitarios.Utils;
import classesAuxiliares.Acao;
import classesAuxiliares.Evolucao;
import classesAuxiliares.MedidaControle;
import classesAuxiliares.Risco;
import classesAuxiliares.TipoRisco;

public class RelatorioSSMAGestaoRiscosService {
	private static final Color COR_LINHAS_IMPARES_EVOLUCAO = new Color(242,242,242);
	private static final Color COR_LINHAS_IMPARES_MEDIDAS_CONTROLE = new Color(242,242,242);
	
	private static final int INDICE_TABELA_CENARIO = 0;
	private static final int INDICE_LINHA_PROCESSO = 1;
	private static final int INDICE_CELULA_PROCESSO = 2;
	private static final int INDICE_LINHA_RESPONSAVEL_APROVADOR_CENARIO = 5;
	private static final int INDICE_CELULA_RESPONSAVEL_CENARIO = 0;
	private static final int INDICE_LINHA_APROVADOR = 5;
	private static final int INDICE_CELULA_APROVADOR = 1;
	private static final int INDICE_LINHA_ASSOCIACAO_CENARIO = 7;
	
	private static final int INDICE_TABELA_RISCO = 1;
	private static final int INDICE_LINHA_RESULTANTE_RISCO = 0;
	private static final int INDICE_CELULA_RESULTANTE_RISCO = 1;
	
	private static final int INDICE_CELULA_CONTEM_TABELA_EVOLUCAO = 1;
	private static final int INDICE_LINHA_TABELA_EVOLUCAO = 5;
	private static final int INDICE_TABELA_EVOLUCAO = 0;
	private static final int INDICE_CELULA_RESULTANTE_EVOLUCAO = 2;
	
	private static final int INDICE_CELULA_CONTEM_TABELA_MEDIDA = 1;
	private static final int INDICE_LINHA_TABELA_MEDIDA_CONTROLE = 6;
	private static final int INDICE_TABELA_MEDIDA_CONTROLE = 0;
	
	private static final int INDICE_TABELA_ACAO = 4;
	private static final int INDICE_LINHA_TIPO_CLASSIFICACAO = 2;
	private static final int INDICE_CELULA_RESPONSAVEL_ACAO = 0;
	private static final int INDICE_CELULA_TIPO = 1;
	private static final int INDICE_CELULA_CLASSIFICACAO = 2;
	private static final int INDICE_LINHA_ORIGEM_RECURSO_AREA_EXECUCAO = 3;
	private static final int INDICE_CELULA_CUSTO_PREVISTO = 0;
	private static final int INDICE_CELULA_ORIGEM_RECURSO = 1;
	private static final int INDICE_CELULA_AREA_EXECUCAO = 2;
	
	
	public byte[] gerarRelatorioByte() throws Exception {
	
		com.aspose.words.License license = new com.aspose.words.License();
		String dataDir = Utils.getDataDir("resource", "licences");
		license.setLicense(dataDir+"Aspose.Words.lic");
		
		Date dataCorrente = new Date();
		String filename = "AIPR_Relatorio.docx";
		Document documento = new Document(Utils.getDataDir("resource", "arquivosModelo") + filename);
		DocumentBuilder builder = new DocumentBuilder(documento);	
		String dataArquivo = Utils.formatarData("dd-MM-yyyy HH-mm-ss", dataCorrente);
		
		
		GestaoRiscosTeste teste = new GestaoRiscosTeste();
		GestaoRiscosDTO dto = teste.gerarDTO();
		
		gerarListaBullets(builder, dto.getLocais(), "listaLocais");
		
		gerarListaBullets(builder, dto.getCenariosAssociados(), "listaCenarios");
		
		Table tabelaCenario = (Table)documento.getChild(NodeType.TABLE, INDICE_TABELA_CENARIO, true);
		
		ajustarConformeConfiguracoesModulo(dto, tabelaCenario, INDICE_TABELA_CENARIO);
		
		String[] fields = new String[]{"codigoCenario", "cenario", "status", "dataCadastro", "processo", "h1", 
				"h2", "h3", "detalhamentoCenario", "matriculaResponsavel", "nomeResponsavel", "matriculaAprovador",
				"nomeAprovador", "numRevisao", "dataAprovacao", "dataRevisao", "registroRestrito"};
		
		Object[]  values = new Object[]{dto.getCodigoCenario(), dto.getCenario(), dto.getStatus(), dto.getDataCadastro(), 
				dto.getProcesso(), dto.getH1(), dto.getH2(), dto.getH3(), dto.getDetalhamentoCenario(), 
				dto.getResponsavel().getMatricula(), dto.getResponsavel().getNome(), dto.getAprovador().getMatricula(), dto.getAprovador().getNome(),
				dto.getRevisao(), dto.getAprovadoEm(), dto.getRevisadoEm(), dto.getRegistroRestrito()};
		
		documento.getMailMerge().execute(fields, values);
		
		inserirRiscosAcoes(documento, builder, dto);
		
		documento.save(Utils.getDataDir("resource", "relatoriosGerados") + dataArquivo + ".pdf");
		return null;
	}
	
	
	/**
	 * M�todo para remover c�lula da tabela.
	 * @param tabela
	 * @param linha
	 * @param celula
	 */
	public void removerCelula(Table tabela, int linha, int celula) {
		tabela.getRows().get(linha).getCells().get(celula).remove();
	}
	
	
	/**
	 * M�todo para alterar largura de c�lula da tabela.
	 * @param tabela
	 * @param linha
	 * @param celula
	 * @param largura
	 */
	public void definirLarguraCelula (Table tabela, int linha, int celula, float largura) {
		tabela.getRows().get(linha).getCells().get(celula).getCellFormat().setWidth(largura);
	}
	
	
	/**
	 * M�todo para remover c�lulas das informa�es do cen�rio, conforme configura��es do m�dulo.
	 * @param dto
	 * @param tabelaDadosCenario
	 * @throws Exception 
	 */
	public void ajustarConformeConfiguracoesModulo(GestaoRiscosDTO dto, Table tabela, int tabelaConfiguracao) throws Exception {
		if (tabelaConfiguracao == INDICE_TABELA_CENARIO) {
			if (dto.isProcessoVisivel() == false) {
				tabela.getRows().get(INDICE_LINHA_PROCESSO).getCells().get(INDICE_CELULA_PROCESSO).getLastParagraph().remove();
			}
			if (dto.getConfiguracaoAprovacao().equalsIgnoreCase("NAO_NECESSITA_DE_APROVACAO")) {
				removerCelula(tabela, INDICE_LINHA_APROVADOR, INDICE_CELULA_APROVADOR);
				definirLarguraCelula(tabela, INDICE_LINHA_RESPONSAVEL_APROVADOR_CENARIO, INDICE_CELULA_RESPONSAVEL_CENARIO, 370);
			}
			if (dto.getConfiguracaoAssociacaoCenarios().equalsIgnoreCase("NAO_NECESSITA_ASSOCIACAO")) {
				tabela.getRows().get(INDICE_LINHA_ASSOCIACAO_CENARIO).remove();
			}
		}
		
		if (tabelaConfiguracao == INDICE_TABELA_ACAO) {
			if (dto.isClassificacaoVisivel() == false) {
				removerCelula(tabela, INDICE_LINHA_TIPO_CLASSIFICACAO, INDICE_CELULA_CLASSIFICACAO);
				removerCelula(tabela, INDICE_LINHA_TIPO_CLASSIFICACAO, INDICE_CELULA_TIPO);
			
				definirLarguraCelula(tabela, INDICE_LINHA_TIPO_CLASSIFICACAO, INDICE_CELULA_RESPONSAVEL_ACAO, 265);
			}
			if (dto.isAreaExecucaoVisivel() != false | dto.isOrigemRecursoVisivel() != false) {
				if (dto.isAreaExecucaoVisivel() == false && dto.isOrigemRecursoVisivel() == false) {
					removerCelula(tabela, INDICE_LINHA_ORIGEM_RECURSO_AREA_EXECUCAO, INDICE_CELULA_AREA_EXECUCAO);
					removerCelula(tabela, INDICE_LINHA_ORIGEM_RECURSO_AREA_EXECUCAO, INDICE_CELULA_ORIGEM_RECURSO);
					
					definirLarguraCelula(tabela, INDICE_LINHA_ORIGEM_RECURSO_AREA_EXECUCAO, INDICE_CELULA_CUSTO_PREVISTO, 530);
				}else {
					if (dto.isAreaExecucaoVisivel() == false) {
						removerCelula(tabela, INDICE_LINHA_ORIGEM_RECURSO_AREA_EXECUCAO, INDICE_CELULA_AREA_EXECUCAO);
					}
					if (dto.isOrigemRecursoVisivel() == false) {
						removerCelula(tabela, INDICE_LINHA_ORIGEM_RECURSO_AREA_EXECUCAO, INDICE_CELULA_ORIGEM_RECURSO);
					}
					definirLarguraCelula(tabela, INDICE_LINHA_ORIGEM_RECURSO_AREA_EXECUCAO, INDICE_CELULA_CUSTO_PREVISTO, 265);
					/*�ndice 1 pois, nesse caso, s� haver� duas celulas na linha. Logo, a segunda deve ser alterada, indiferente se 
						� de origem ou area de execu��o. */
					definirLarguraCelula(tabela, INDICE_LINHA_ORIGEM_RECURSO_AREA_EXECUCAO, 1, 265);
				}
			}
		}
	}
		
	
	/**
	 * M�todo para gerar lista com bullets.
	 * @param builder
	 * @param listaItens
	 * @param bookmark
	 * @throws Exception
	 */
	public void gerarListaBullets(DocumentBuilder builder, List<String> itensLista, String bookmark) throws Exception {
		builder.moveToBookmark(bookmark);
		if (itensLista == null) {
			builder.write("-");
		}else {
			builder.getListFormat().applyBulletDefault();
			builder.getListFormat().getListLevel().setNumberPosition(0);
			builder.getListFormat().getListLevel().setTabPosition(10);
			
			int contadorRepeti��esLocais = 1;
			for(String local : itensLista){
				if (contadorRepeti��esLocais < (itensLista.size())) {
					builder.writeln(local);
				}else {
					builder.write(local);
				}
				contadorRepeti��esLocais++;
			}
		}
	}
	
	
	/**
	 * Metodo para Inserir Riscos e suas respectivas a��es.
	 * @param doc
	 * @param builder
	 * @param dto
	 * @throws Exception
	 */
	public void inserirRiscosAcoes(Document documento, DocumentBuilder builder, GestaoRiscosDTO dto) throws Exception {
		try {
			Table tabelaRiscosModelo = (Table)documento.getChild(NodeType.TABLE, INDICE_TABELA_RISCO, true);
			Table tabelaAcoesModelo = (Table)documento.getChild(NodeType.TABLE, INDICE_TABELA_ACAO, true);
			List<Risco> listaRiscos = dto.getRiscos();
			
			if (listaRiscos.size() == 0) {
				tabelaRiscosModelo.remove();
				tabelaAcoesModelo.remove();
			}else {
				ajustarConformeConfiguracoesModulo(dto, tabelaAcoesModelo, INDICE_TABELA_ACAO);
				
				Table tabelaRiscosModeloClone = (Table) tabelaRiscosModelo.deepClone(true);
				Table tabelaAcoesModeloClone = (Table) tabelaAcoesModelo.deepClone(true);
				Table tabelaReferencia = tabelaRiscosModelo;
				Table ultimaTabelaRiscoDocumento = tabelaRiscosModelo;
				boolean tabelaAcoesModeloUsada = false;
				
				int contadorRiscosInseridos = 1;
				for (int contRiscos = 0; contRiscos < listaRiscos.size(); contRiscos++){
					Risco risco = listaRiscos.get(contRiscos);
					
					if (contRiscos != 0) {
						if (tabelaAcoesModeloUsada == false) {
							tabelaReferencia = ultimaTabelaRiscoDocumento;
						}
						ultimaTabelaRiscoDocumento = clonarInserirTabela(tabelaRiscosModeloClone.deepClone(true), tabelaReferencia);
						tabelaReferencia= ultimaTabelaRiscoDocumento;
						contadorRiscosInseridos +=1;
						inserirParagrafoAntesTabela(ultimaTabelaRiscoDocumento, documento);
					}
		
					
					inserirDadosTabelaRisco(builder, risco, ultimaTabelaRiscoDocumento, contadorRiscosInseridos);
					
					if (risco.getAcoesRisco() != null) {
						List<Acao> acoesRisco = risco.getAcoesRisco();
						for (int i = 0; i < acoesRisco.size(); i++) {
							Acao acao = acoesRisco.get(i);
							
							if (tabelaAcoesModeloUsada == true) {
								tabelaReferencia = clonarInserirTabela(tabelaAcoesModeloClone, tabelaReferencia);
								inserirParagrafoAntesTabela(tabelaReferencia, documento);
							}else {
								tabelaReferencia = tabelaAcoesModelo;
							}
							
							inserirDadosTabelaAcoes(acao, tabelaReferencia);
							tabelaAcoesModeloUsada = true;
						}
					}
				}
				if (tabelaAcoesModeloUsada == false) {
					tabelaAcoesModelo.remove();
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * M�todo para inserir par�grafo sem texto antes da tabela.
	 * @param tabela
	 * @param paragrafoModelo
	 * @return
	 */
	public Paragraph inserirParagrafoAntesTabela(Table tabela, Document documento) {
		Paragraph paragrafoSemTexto = new Paragraph(documento);
		Paragraph paragrafoInserido = (Paragraph) tabela.getParentNode().insertBefore(paragrafoSemTexto.deepClone(true), tabela);
		return paragrafoInserido;
	}
	
	
	/**
	 * M�todo para transformar objeto Risco em List<String>.
	 * @param risco
	 * @return List<String> dadosRisco
	 */
	public List<String> obterDadosRisco(Risco risco) {
		List<String> dadosRisco = new ArrayList<String>();
		
		TipoRisco tipoRisco = risco.getTipoRisco();
		dadosRisco.add(risco.getResultanteRisco());
		dadosRisco.add(tipoRisco.getDescricaoTipoRisco());
		dadosRisco.add(tipoRisco.getIdentVar1());
		dadosRisco.add(risco.getIdentVar1());
		dadosRisco.add(tipoRisco.getIdentVar2());
		dadosRisco.add(risco.getIdentVar2());
		dadosRisco.add(tipoRisco.getMatrizVar1());
		dadosRisco.add(tipoRisco.getMatrizVar2());
		dadosRisco.add(risco.getGrupoControle());
		dadosRisco.add(risco.getSubgrupoControle());
		dadosRisco.add(risco.getMoedaPerdaPotencial());
		dadosRisco.add(risco.getValorPerdaPotencial());
		dadosRisco.add(risco.getMemoriaCalculo());
				
		return dadosRisco;
	}
	
	
	/**
	 * M�todo para inserir informa��es na tabela de Risco.
	 * @param doc
	 * @param risco
	 * @param ultimaTabelaRiscoInserida
	 * @param tabelaRiscosModelo
	 * @throws Exception
	 */
	public void inserirDadosTabelaRisco(DocumentBuilder builder, Risco risco, Table tabela, int numAvaliacao) throws Exception {
		
		String listaMarcadoresRisco[] = {"<resultanteRisco>", "<tipoRisco>", "<lb_ident_Var1>", "<dadosIdentVar1>", "<lb_ident_Var2>", 
				"<dadosIdentVar2>", "<lb_Matriz_Var1>", "<lb_Matriz_Var2>", "<grupoControle>", "<subgrupoControle>", "<moedaPotencial>",
				"<valorPotencial>", "<memoriaCalculo>"};
	
		List<String> detalhesRisco = obterDadosRisco(risco);
		
		alterarCorCelula(tabela, 0, INDICE_CELULA_RESULTANTE_RISCO, risco.getCorResultanteRisco());
		
		ajustarCorTextoConformeLuminosidade(risco.getCorResultanteRisco(), tabela.getRows().get(INDICE_LINHA_RESULTANTE_RISCO).getCells().get(INDICE_CELULA_RESULTANTE_RISCO));
		
		//Tamb�m altera labels das tabelas de Hist�rico das Avalia��es e Medidas de Controle
		substituirMarcadoresTabela(listaMarcadoresRisco, tabela, detalhesRisco);
		tabela.getRange().replace("<num_avaliacao>", Integer.toString(numAvaliacao), new FindReplaceOptions());
			
		List<Evolucao> evolucoesRisco = risco.getHistoricoEvolucao();
		inserirEvolucoes(builder, evolucoesRisco, tabela);
		
		Cell celulaComTabelaMedidasControle= tabela.getRows().get(INDICE_LINHA_TABELA_MEDIDA_CONTROLE).getCells().get(INDICE_CELULA_CONTEM_TABELA_MEDIDA);
		Table tabelaMedidas = (Table) celulaComTabelaMedidasControle.getChild(NodeType.TABLE, INDICE_TABELA_MEDIDA_CONTROLE, true);
		
		if (risco.getMedidasControle() != null) {
			List<MedidaControle> medidasControleRisco = risco.getMedidasControle();
			inserirMedidasControle(medidasControleRisco, tabelaMedidas);
		}else {
			tabelaMedidas.getLastRow().getCells().clear();
		}
	}
	
	
	/**
	 * M�todo para transformar objeto Evolu��o em List<String>
	 * @param evolucao
	 * @return List<String> dadosEvolucao
	 */
	public List<String> obterDadosEvolucao (Evolucao evolucao){
		List <String> dadosEvolucao = new ArrayList<String>();
		dadosEvolucao.add(evolucao.getMatrizVar1());
		dadosEvolucao.add(evolucao.getMatrizVar2());
		dadosEvolucao.add(evolucao.getResultante());
		dadosEvolucao.add(evolucao.getData());
		dadosEvolucao.add(evolucao.getResponsavel().getMatricula());
		dadosEvolucao.add(evolucao.getResponsavel().getNome());
		
		return dadosEvolucao;
	}
	
	
	/**
	 * M�todo para inserir Evolu��es na respectiva tabela para o respectivo risco.
	 * @param listaEvolucoesRisco
	 * @param ultimaTabelaRiscoInserida
	 * @throws Exception
	 */
	public void inserirEvolucoes (DocumentBuilder builder, List<Evolucao> listaEvolucoes, Table tabela) throws Exception {	
		Cell celulaComTabelaEvolucao = tabela.getRows().get(INDICE_LINHA_TABELA_EVOLUCAO).getCells().get(INDICE_CELULA_CONTEM_TABELA_EVOLUCAO);
		Table tabelaEvolucao = (Table) celulaComTabelaEvolucao.getChild(NodeType.TABLE, INDICE_TABELA_EVOLUCAO, true);
		
		Row linhaModelo = (Row) tabelaEvolucao.getLastRow().deepClone(true);
		String marcadores[] = {"<matrizVar1Historico>", "<matrizVar2Historico>", "<nrHistorico>", "<dataHistorico>",
				"<matriculaRespHistorico>", "<nomeResponsavelHistorico>"};
		
		int contadorLinhasEvolucao = 1;
		for (int i = 0; i < listaEvolucoes.size(); i++) {
			
			if (i != 0) {
				contadorLinhasEvolucao = clonarLinha(tabelaEvolucao, linhaModelo, COR_LINHAS_IMPARES_EVOLUCAO, contadorLinhasEvolucao);
			}
			
			substituirMarcadoresTabela(marcadores, tabela, obterDadosEvolucao(listaEvolucoes.get(i)));
			
			alterarCorCelula(tabelaEvolucao, contadorLinhasEvolucao, INDICE_CELULA_RESULTANTE_EVOLUCAO, listaEvolucoes.get(i).getCorResultante());
			
			Cell celulaResultante = tabelaEvolucao.getRows().get(contadorLinhasEvolucao).getCells().get(INDICE_CELULA_RESULTANTE_EVOLUCAO);
			 
			if (listaEvolucoes.get(i).isDesejada() == true) {
				definirEvolucaoDesejada(builder, celulaResultante);
			}
			
			ajustarCorTextoConformeLuminosidade(listaEvolucoes.get(i).getCorResultante(), (Cell) celulaResultante);
		}
	}
	
	
	/**
	 * M�todo para inserir �cone star na c�lula da evolu��o desejada.
	 * @param evolucaoDesejada
	 * @param celula
	 * @throws Exception
	 */
	public void definirEvolucaoDesejada(DocumentBuilder builder, Cell celula) {
		Paragraph paragrafo = celula.getFirstParagraph();
		builder.moveTo(paragrafo);
		ajustarFonteBuilder(builder, "Wingdings", 10, Color.BLACK);
		builder.write("\u00AB");
	}


	/**
	 * M�todo para ajustar fonte utilizada pelo builder.
	 * @param builder
	 * @param nomeFonte
	 * @param tamanhoFonte
	 */
	public void ajustarFonteBuilder(DocumentBuilder builder, String nomeFonte, int tamanhoFonte, Color cor) {
		builder.getFont().setName(nomeFonte);
		builder.getFont().setSize(tamanhoFonte);
		builder.getFont().setColor(cor);
	}
	
	
	/**
	 * M�todo para alterar cor do texto da c�lula conforme luminosidade da cor de fundo da c�lula.
	 * @param cor
	 * @param celula
	 */
	public void ajustarCorTextoConformeLuminosidade(String corCelula, Cell celula) {
		float luminosidade = calcularLuminosidadeCor(corCelula);
		if (luminosidade < 160) {
			for (Iterator<Run> iterator = (Iterator<Run>)celula.getChildNodes(NodeType.RUN, true).iterator(); iterator.hasNext();) {
				Run run = iterator.next();
				run.getFont().setColor(Color.WHITE);
			}
		}
	}
	
	
	/**
	 * M�todo para calcular luminosidade da cor de fundo da c�lula.
	 * @param cor
	 * @return float luminosidade
	 */
	public float calcularLuminosidadeCor(String cor) {
		//C�lculo da luminosidade da cor de fundo da c�lula de avalia��es.
		Color corConvertida = Color.decode(cor);
		float luminosidade = ( corConvertida.getRed() * 299 + corConvertida.getGreen() * 587 + corConvertida.getBlue() * 114) / 1000;
		return luminosidade;
	}
	
	
	/**
	 * M�todo para transnformar objeto MedidaControle em List<String>;
	 * @param medidaControle
	 * @return List<String> dadosMedidaControle
	 */
	public List<String> obterDadosMedidaControle (MedidaControle medidaControle){
		List <String> dadosMedidaControle = new ArrayList<String>();
		dadosMedidaControle.add(medidaControle.getMedidaControle());
		dadosMedidaControle.add(medidaControle.getMatrizVar1());
		dadosMedidaControle.add(medidaControle.getMatrizVar2());
		dadosMedidaControle.add(medidaControle.getData());
		dadosMedidaControle.add(medidaControle.getResponsavel().getMatricula());
		dadosMedidaControle.add(medidaControle.getResponsavel().getNome());
		
		return dadosMedidaControle;
	}
	
	
	/**
	 * M�todo para inserir Medidas de Controle na respectiva tabela para o respectivo risco.
	 * @param listaMedidasControleRisco
	 * @param ultimaTabelaRiscoInserida
	 * @throws Exception
	 */
	public void inserirMedidasControle (List<MedidaControle> listaMedidas, Table tabela) throws Exception {	
		Row linhaModelo = (Row) tabela.getLastRow().deepClone(true);
		
		String listaMarcadoresMedidasControle[] = {"<medidaControle>", "<matrizVar1Marcado>", "<matrizVar2Marcado>", 
				"<dataMedida>", "<matriculaRespMedida>", "<nomeRespMedida>"};

		int contadorLinhasMedidaControle = 1;
		for (int i = 0; i < listaMedidas.size(); i++) {
			
			if (i != 0) {
				contadorLinhasMedidaControle = clonarLinha(tabela, linhaModelo, COR_LINHAS_IMPARES_MEDIDAS_CONTROLE, contadorLinhasMedidaControle);
			}
			substituirMarcadoresTabela(listaMarcadoresMedidasControle, tabela, obterDadosMedidaControle(listaMedidas.get(i)));
		}
	}
	
	
	/**
	 * M�todo para transformar objeto Acao em List<String>.
	 * @param acao
	 * @return
	 */
	public List<String> obterDadosAcao (Acao acao) {
		List <String> dadosAcao = new ArrayList<String>();
		dadosAcao.add(acao.getCodigo());
		dadosAcao.add(acao.getStatus());
		dadosAcao.add(acao.getPrioridade());
		dadosAcao.add(acao.getPrevisao());
		dadosAcao.add(acao.getDescricao());
		dadosAcao.add(acao.getResponsavel().getMatricula());
		dadosAcao.add(acao.getResponsavel().getNome());
		dadosAcao.add(acao.getTipo());
		dadosAcao.add(acao.getClassificacao());
		dadosAcao.add(acao.getAcaoRecorrente());
		dadosAcao.add(acao.getMoedaCustoPrevisto());
		dadosAcao.add(acao.getValorCustoPrevisto());
		dadosAcao.add(acao.getOrigemRecurso());
		dadosAcao.add(acao.getAreaExecucao());
		dadosAcao.add(acao.getDescricaoImplantacao());
		dadosAcao.add(acao.getMoedaCustoReal());
		dadosAcao.add(acao.getValorCustoReal());
		dadosAcao.add(acao.getDataImplantacao());
		
		return dadosAcao;
	}
	
	
	/**
	 * Inserir informa��es na tabela de a��es.
	 * @param acao
	 * @param ultimaTabelaInserida
	 * @throws Exception
	 */
	public void inserirDadosTabelaAcoes(Acao acao, Table tabela) throws Exception {
		String listaMarcadoresAcao[] = {"<codigoAcao>", "<statusAcao>", "<prioridade>", "<previsao>", "<descricao>", 
				"<matriculaResponsavelAc>", "<nomeResponsavelAc>", "<tipo>", "<classificacao>", "<acaoRecorrente>", "<moedaPrev>",
				"<valorPrev>","<origemRecurso>", "<areaExecucao>", "<descricaoImplantacao>", "<moedaReal>", 
				"<valorReal>", "<dataImp>"};
		
		List<String> dadosAcao = obterDadosAcao(acao);
		
		substituirMarcadoresTabela(listaMarcadoresAcao, tabela, dadosAcao);
		
	}
	
	
	/**
	 * Metodo para clonar e inserir tabelas no documento.
	 * @param tabelaMolde
	 * @param ultimoItemInserido
	 * @return tabelaAdicionada
	 * @throws Exception 
	 */
	public Table clonarInserirTabela(Node tabelaMolde, Node ultimaTabela){
		Table tabelaCloneMolde = (Table) tabelaMolde.deepClone(true);
		Table tabelaAdicionada = (Table) ultimaTabela.getParentNode().insertAfter(tabelaCloneMolde, ultimaTabela);
		
		return tabelaAdicionada;
	}
	
	
	/**
	 * M�todo para clonar e inserir linha ao final da tabela.
	 * @param tabela
	 * @param linhaMolde
	 * @param corLinhasImpares
	 * @param contadorLinhas
	 * @return
	 */
	public int clonarLinha(Table tabela, Row linhaMolde, Color corLinhasImpares, int contadorLinhas) {
		Row linhaInserida = null;
		Row linhaClonada = (Row) linhaMolde.deepClone(true);
		linhaInserida = (Row) tabela.appendChild(linhaClonada);
		contadorLinhas += 1;
			
		if (contadorLinhas % 2 == 0) {
			for (int j = 0; j <5; j++) {
				linhaInserida.getCells().get(j).getCellFormat().getShading().setBackgroundPatternColor(corLinhasImpares);
			}
		}
		
		return contadorLinhas;
	}
	
	
	/**
	 * M�todo para substituir os marcadores da tabela pelas informa��es.
	 * @param listaMarcadores
	 * @param tabelaAcaoMarcadores
	 * @param detalhesAcao
	 * @throws Exception
	 */
	public void substituirMarcadoresTabela(String[] listaMarcadores, Table tabela, List<String> itens) throws Exception {
		for (int i = 0; i < listaMarcadores.length; i++) {
			tabela.getRange().replace(listaMarcadores[i], itens.get(i), new FindReplaceOptions());
		}
	}
	
	
	/**
	 * M�todo para alterar cor de fundo da c�lula.
	 * @param tabela
	 * @param linha
	 * @param celula
	 * @param corHex
	 */
	public void alterarCorCelula(Table tabela, int linha, int celula, String corHex) {
		Color corConvertida = Color.decode(corHex); 
		Cell celulaCor = tabela.getRows().get(linha).getCells().get(celula);
		celulaCor.getCellFormat().getShading().setBackgroundPatternColor(corConvertida);
	}
}
