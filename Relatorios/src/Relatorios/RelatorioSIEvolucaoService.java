package Relatorios;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;
import com.aspose.words.Document;
import com.aspose.words.DocumentBuilder;
import com.aspose.words.LineStyle;
import com.aspose.words.SaveFormat;
import com.aspose.words.Table;
import DTOs.EvolucaoDTO;
import Utilitarios.ConsultaAPI;
import Utilitarios.Utils;

/** 
 * sClasse para gera��o de relat�rios de evolu��o m�dica - m�dulo SI.
 * @author Gustavo Preto Benvenutti 
 */

public class RelatorioSIEvolucaoService {	
		
	/** M�todo para gera��o do relat�rio
	 * 
	 * @return byte[] - ap�s salvar arquivo
	 * @throws Exception
	 */
	public byte[] gerarRelatorioEvolucao() throws Exception {
		
		//List<String> resultado = new ArrayList<>();
		com.aspose.words.License license = new com.aspose.words.License();
		String dataDir = Utils.getDataDir("resource", "licences");
		license.setLicense(dataDir+"Aspose.Words.lic");
		
		Date dataCorrente = new Date();
		
		String filename = "Padrao.docx";
		Document doc = new Document(Utils.getDataDir("resource", "arquivosModelo") + filename);
		
		DocumentBuilder builder = new DocumentBuilder(doc);	
		
		String dataArquivo = Utils.formatarData("dd-MM-yyyy HH-mm-ss", dataCorrente);
		String dataCabecalhoRelatorio = Utils.formatarData("dd/MM/yyyy", dataCorrente);
		
		builder.moveToBookmark("imagem_Logo");
		String imageFileName = Utils.getDataDir("resource", "logos") + "LogoApollus.png";
		builder.insertImage(imageFileName);
		
		builder.moveToBookmark("pagina_Cabecalho");
		builder.insertField("PAGE", null);
		builder.write(" de ");
		builder.insertField("NUMPAGES", null);
		
		ConsultaAPI consulta = new ConsultaAPI();
		List<org.bson.Document> resultadosConsultaAPI = consulta.executar();
		
		EvolucaoDTO dto = new EvolucaoDTO(resultadosConsultaAPI);
		
		String[] fields = new String[]{"dataRelatorio","nomeFuncionario", "dataNascimentoFuncionario", "matriculaFuncionario", "h1Funcionario", 
				"identidadeFuncionario", "cargoFuncionario"};
		Object[]  values = new Object[]{dataCabecalhoRelatorio
				,dto.getNome(), dto.getDataNasc(), dto.getMatricula(), dto.getUnidade(), dto.getIdentidade(), dto.getCargo()}; 
		doc.getMailMerge().execute(fields, values);
		
		//Mover para parte abaixo das informa��es e iniciar tabelas
		builder.moveToBookmark("primeira_Tabela");
		
		for (int i = 0; i < dto.getItem2().size(); i++) {	    
		    Table tabelaConteudo = builder.startTable();
		    builder.insertCell();
		    	Utils.formatarTabela(tabelaConteudo, 100.4, 2);
		    	tabelaConteudo.setBorders(LineStyle.SINGLE, 3.0, Color.WHITE);
		    	Utils.formatarCelula(builder, 37.6, 2, 1, 1, 2, new Color(217, 217, 217));	    	
		    	Utils.formatarParagrafo(builder, 0, 15.4);
		    	Utils.configurarFonte(builder, "ARIAL", 10, true);
		    	builder.writeln(dto.getItem1().get(i));
		    	Utils.configurarFonte(builder, "ARIAL", 9, false);
		    	builder.writeln(dto.getItem2().get(i));
		    	builder.writeln(dto.getItem3().get(i));
		    	builder.writeln(dto.getItem1().get(i));
		    	builder.write(dto.getItem2().get(i));
		    	
		    builder.insertCell();
		    	Utils.formatarCelula(builder, 0.5, 0, 0, 1, 2, Color.WHITE);
		    	
		    builder.insertCell();
		    	Utils.formatarCelula(builder, 61.9, 2, 1, 1, 2, new Color(217, 217, 217));
		    	tabelaConteudo.setBorders(LineStyle.NONE, 0.2, Color.WHITE);
		    	builder.writeln(dto.getItem1().get(i));
		    	builder.writeln(dto.getItem2().get(i));
		    	builder.write(dto.getItem3().get(i));
		    builder.endRow();
		    builder.endTable();
		    
		    Utils.configurarFonte(builder, "ARIAL", 7, false);
		    builder.writeln("");
		 
		 }
		/*
		builder.insertBreak(BreakType.SECTION_BREAK_NEW_PAGE);
		builder.getPageSetup().setOrientation(Orientation.LANDSCAPE);
	
		builder.writeln("Teste mudanca orientacao da Pagina!");
		*/
		
		ByteArrayOutputStream docOutStream = new ByteArrayOutputStream();
		doc.save(docOutStream, SaveFormat.DOC);
		byte[] docBytes = docOutStream.toByteArray();
		doc.save(Utils.getDataDir("resource", "relatoriosGerados") + dataArquivo + ".pdf");
		return docBytes;
	}
	
}
