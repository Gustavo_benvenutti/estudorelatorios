package classesAuxiliares;

public class TipoRisco {
	public String descricaoTipoRisco;
	public String identVar1;
	public String identVar2;
	public String matrizVar1;
	public String matrizVar2;
	
	
	public String getDescricaoTipoRisco() {
		return descricaoTipoRisco;
	}
	public void setDescricaoTipoRisco(String descricaoTipoRisco) {
		this.descricaoTipoRisco = descricaoTipoRisco;
	}
	public String getIdentVar1() {
		return identVar1;
	}
	public void setIdentVar1(String identVar1) {
		this.identVar1 = identVar1;
	}
	public String getIdentVar2() {
		return identVar2;
	}
	public void setIdentVar2(String identVar2) {
		this.identVar2 = identVar2;
	}
	public String getMatrizVar1() {
		return matrizVar1;
	}
	public void setMatrizVar1(String matrizVar1) {
		this.matrizVar1 = matrizVar1;
	}
	public String getMatrizVar2() {
		return matrizVar2;
	}
	public void setMatrizVar2(String matrizVar2) {
		this.matrizVar2 = matrizVar2;
	}
}
