package classesAuxiliares;

public class Evolucao {
	public String matrizVar1;
	public String matrizVar2;
	public String resultante;
	public String corResultante;
	public boolean desejada;
	public String data;
	public Funcionario responsavel;
	
	
	public String getMatrizVar1() {
		return matrizVar1;
	}
	public void setMatrizVar1(String matrizVar1) {
		this.matrizVar1 = matrizVar1;
	}
	public String getMatrizVar2() {
		return matrizVar2;
	}
	public void setMatrizVar2(String matrizVar2) {
		this.matrizVar2 = matrizVar2;
	}
	public String getResultante() {
		return resultante;
	}
	public void setResultante(String resultante) {
		this.resultante = resultante;
	}
	public String getCorResultante() {
		return corResultante;
	}
	public void setCorResultante(String corResultante) {
		this.corResultante = corResultante;
	}
	public boolean isDesejada() {
		return desejada;
	}
	public void setDesejada(boolean desejada) {
		this.desejada = desejada;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public Funcionario getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(Funcionario responsavel) {
		this.responsavel = responsavel;
	}
	
}
