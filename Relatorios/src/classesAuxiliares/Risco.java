package classesAuxiliares;

import java.util.List;

public class Risco {
	public String resultanteRisco;
	public String corResultanteRisco;
	public TipoRisco tipoRisco;
	public String identVar1;
	public String identVar2;
	public String grupoControle;
	public String subgrupoControle;
	public String moedaPerdaPotencial;
	public String valorPerdaPotencial;
	public String memoriaCalculo;
	public List<Evolucao> historicoEvolucao;
	public List<MedidaControle> medidasControle;
	public List<Acao> acoesRisco;
	
	public String getResultanteRisco() {
		return resultanteRisco;
	}
	public void setResultanteRisco(String resultanteRisco) {
		this.resultanteRisco = resultanteRisco;
	}
	public String getCorResultanteRisco() {
		return corResultanteRisco;
	}
	public void setCorResultanteRisco(String corResultanteRisco) {
		this.corResultanteRisco = corResultanteRisco;
	}
	public TipoRisco getTipoRisco() {
		return tipoRisco;
	}
	public void setTipoRisco(TipoRisco tipoRisco) {
		this.tipoRisco = tipoRisco;
	}
	public String getIdentVar1() {
		return identVar1;
	}
	public void setIdentVar1(String identVar1) {
		this.identVar1 = identVar1;
	}
	public String getIdentVar2() {
		return identVar2;
	}
	public void setIdentVar2(String identVar2) {
		this.identVar2 = identVar2;
	}
	public String getGrupoControle() {
		return grupoControle;
	}
	public void setGrupoControle(String grupoControle) {
		this.grupoControle = grupoControle;
	}
	public String getSubgrupoControle() {
		return subgrupoControle;
	}
	public void setSubgrupoControle(String subgrupoControle) {
		this.subgrupoControle = subgrupoControle;
	}
	public String getMoedaPerdaPotencial() {
		return moedaPerdaPotencial;
	}
	public void setMoedaPerdaPotencial(String moedaPerdaPotencial) {
		this.moedaPerdaPotencial = moedaPerdaPotencial;
	}
	public String getValorPerdaPotencial() {
		return valorPerdaPotencial;
	}
	public void setValorPerdaPotencial(String valorPerdaPotencial) {
		this.valorPerdaPotencial = valorPerdaPotencial;
	}
	public String getMemoriaCalculo() {
		return memoriaCalculo;
	}
	public void setMemoriaCalculo(String memoriaCalculo) {
		this.memoriaCalculo = memoriaCalculo;
	}
	public List<Evolucao> getHistoricoEvolucao() {
		return historicoEvolucao;
	}
	public void setHistoricoEvolucao(List<Evolucao> historicoEvolucao) {
		this.historicoEvolucao = historicoEvolucao;
	}
	public List<MedidaControle> getMedidasControle() {
		return medidasControle;
	}
	public void setMedidasControle(List<MedidaControle> medidasControle) {
		this.medidasControle = medidasControle;
	}
	public List<Acao> getAcoesRisco() {
		return acoesRisco;
	}
	public void setAcoesRisco(List<Acao> acoesRisco) {
		this.acoesRisco = acoesRisco;
	}
}


