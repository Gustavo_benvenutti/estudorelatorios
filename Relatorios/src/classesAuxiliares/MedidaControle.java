package classesAuxiliares;

public class MedidaControle {
	public String medidaControle;
	public String matrizVar1;
	public String matrizVar2;
	public String data;
	public Funcionario responsavel;
	
	public String getMedidaControle() {
		return medidaControle;
	}
	public void setMedidaControle(String medidaControle) {
		this.medidaControle = medidaControle;
	}
	public String getMatrizVar1() {
		return matrizVar1;
	}
	public void setMatrizVar1(String matrizVar1) {
		this.matrizVar1 = matrizVar1;
	}
	public String getMatrizVar2() {
		return matrizVar2;
	}
	public void setMatrizVar2(String matrizVar2) {
		this.matrizVar2 = matrizVar2;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public Funcionario getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(Funcionario responsavel) {
		this.responsavel = responsavel;
	}
	
}
