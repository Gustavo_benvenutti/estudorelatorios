package classesAuxiliares;

public class Acao {
	public String codigo;
	public String status;
	public String prioridade;
	public String previsao;
	public String descricao;
	public Funcionario responsavel;
	public String tipo;
	public String classificacao;
	public String acaoRecorrente;
	public String moedaCustoPrevisto;
	public String valorCustoPrevisto;
	public String origemRecurso;
	public String areaExecucao;
	public String descricaoImplantacao;
	public String moedaCustoReal;
	public String valorCustoReal;
	public String dataImplantacao;
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPrioridade() {
		return prioridade;
	}
	public void setPrioridade(String prioridade) {
		this.prioridade = prioridade;
	}
	public String getPrevisao() {
		return previsao;
	}
	public void setPrevisao(String previsao) {
		this.previsao = previsao;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Funcionario getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(Funcionario responsavel) {
		this.responsavel = responsavel;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getClassificacao() {
		return classificacao;
	}
	public void setClassificacao(String classificacao) {
		this.classificacao = classificacao;
	}
	public String getAcaoRecorrente() {
		return acaoRecorrente;
	}
	public void setAcaoRecorrente(String acaoRecorrente) {
		this.acaoRecorrente = acaoRecorrente;
	}
	public String getMoedaCustoPrevisto() {
		return moedaCustoPrevisto;
	}
	public void setMoedaCustoPrevisto(String moedaCustoPrevisto) {
		this.moedaCustoPrevisto = moedaCustoPrevisto;
	}
	public String getValorCustoPrevisto() {
		return valorCustoPrevisto;
	}
	public void setValorCustoPrevisto(String valorCustoPrevisto) {
		this.valorCustoPrevisto = valorCustoPrevisto;
	}
	public String getOrigemRecurso() {
		return origemRecurso;
	}
	public void setOrigemRecurso(String origemRecurso) {
		this.origemRecurso = origemRecurso;
	}
	public String getAreaExecucao() {
		return areaExecucao;
	}
	public void setAreaExecucao(String areaExecucao) {
		this.areaExecucao = areaExecucao;
	}
	public String getDescricaoImplantacao() {
		return descricaoImplantacao;
	}
	public void setDescricaoImplantacao(String descricaoImplantacao) {
		this.descricaoImplantacao = descricaoImplantacao;
	}
	public String getMoedaCustoReal() {
		return moedaCustoReal;
	}
	public void setMoedaCustoReal(String moedaCustoReal) {
		this.moedaCustoReal = moedaCustoReal;
	}
	public String getValorCustoReal() {
		return valorCustoReal;
	}
	public void setValorCustoReal(String valorCustoReal) {
		this.valorCustoReal = valorCustoReal;
	}
	public String getDataImplantacao() {
		return dataImplantacao;
	}
	public void setDataImplantacao(String dataImplantacao) {
		this.dataImplantacao = dataImplantacao;
	}
	
	
}
