package DTOs;


import java.util.List;
import classesAuxiliares.Funcionario;
import classesAuxiliares.Risco;
/**
* Classe para retornar dados para inser��o do arquivo
* @author Gustavo Preto Benvenutti
* @param consultaAPI - dados para serem processados.
*/

public class GestaoRiscosDTO {
	
	private String codigoCenario;
	
	private String configuracaoAprovacao;
	private boolean processoVisivel;
	private String configuracaoAssociacaoCenarios;
	private boolean classificacaoVisivel;
	private boolean origemRecursoVisivel;
	private boolean areaExecucaoVisivel;
	
	private String cenario;
	private String status;
	private String dataCadastro;
	private String processo;
	private String h1;
	private String h2;
	private String h3;
	private List<String> locais;
	private String detalhamentoCenario;
	private Funcionario responsavel;
	private Funcionario aprovador;
	private String revisao;
	private String aprovadoEm;
	private String revisadoEm;
	private String registroRestrito;
	private List<String> cenariosAssociados;
	private List<Risco> riscos;
	
	
	public String getCodigoCenario() {
		return codigoCenario;
	}
	public void setCodigoCenario(String codigoCenario) {
		this.codigoCenario = codigoCenario;
	}
	public String getConfiguracaoAprovacao() {
		return configuracaoAprovacao;
	}
	public void setConfiguracaoAprovacao(String configuracaoAprovacao) {
		this.configuracaoAprovacao = configuracaoAprovacao;
	}
	public boolean isProcessoVisivel() {
		return processoVisivel;
	}
	public void setProcessoVisivel(boolean processoVisivel) {
		this.processoVisivel = processoVisivel;
	}
	public String getConfiguracaoAssociacaoCenarios() {
		return configuracaoAssociacaoCenarios;
	}
	public void setConfiguracaoAssociacaoCenarios(String configuracaoAssociacaoCenarios) {
		this.configuracaoAssociacaoCenarios = configuracaoAssociacaoCenarios;
	}
	public boolean isClassificacaoVisivel() {
		return classificacaoVisivel;
	}
	public void setClassificacaoVisivel(boolean classificacaoVisivel) {
		this.classificacaoVisivel = classificacaoVisivel;
	}
	public boolean isOrigemRecursoVisivel() {
		return origemRecursoVisivel;
	}
	public void setOrigemRecursoVisivel(boolean origemRecursoVisivel) {
		this.origemRecursoVisivel = origemRecursoVisivel;
	}
	public boolean isAreaExecucaoVisivel() {
		return areaExecucaoVisivel;
	}
	public void setAreaExecucaoVisivel(boolean areaExecucaoVisivel) {
		this.areaExecucaoVisivel = areaExecucaoVisivel;
	}
	public String getCenario() {
		return cenario;
	}
	public void setCenario(String cenario) {
		this.cenario = cenario;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(String dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public String getProcesso() {
		return processo;
	}
	public void setProcesso(String processo) {
		this.processo = processo;
	}
	public String getH1() {
		return h1;
	}
	public void setH1(String h1) {
		this.h1 = h1;
	}
	public String getH2() {
		return h2;
	}
	public void setH2(String h2) {
		this.h2 = h2;
	}
	public String getH3() {
		return h3;
	}
	public void setH3(String h3) {
		this.h3 = h3;
	}
	public List<String> getLocais() {
		return locais;
	}
	public void setLocais(List<String> locais) {
		this.locais = locais;
	}
	public String getDetalhamentoCenario() {
		return detalhamentoCenario;
	}
	public void setDetalhamentoCenario(String detalhamentoCenario) {
		this.detalhamentoCenario = detalhamentoCenario;
	}
	public Funcionario getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(Funcionario responsavel) {
		this.responsavel = responsavel;
	}
	public Funcionario getAprovador() {
		return aprovador;
	}
	public void setAprovador(Funcionario aprovador) {
		this.aprovador = aprovador;
	}
	public String getRevisao() {
		return revisao;
	}
	public void setRevisao(String revisao) {
		this.revisao = revisao;
	}
	public String getAprovadoEm() {
		return aprovadoEm;
	}
	public void setAprovadoEm(String aprovadoEm) {
		this.aprovadoEm = aprovadoEm;
	}
	public String getRevisadoEm() {
		return revisadoEm;
	}
	public void setRevisadoEm(String revisadoEm) {
		this.revisadoEm = revisadoEm;
	}
	public String getRegistroRestrito() {
		return registroRestrito;
	}
	public void setRegistroRestrito(String registroRestrito) {
		this.registroRestrito = registroRestrito;
	}
	public List<String> getCenariosAssociados() {
		return cenariosAssociados;
	}
	public void setCenariosAssociados(List<String> cenariosAssociados) {
		this.cenariosAssociados = cenariosAssociados;
	}
	public List<Risco> getRiscos() {
		return riscos;
	}
	public void setRiscos(List<Risco> riscos) {
		this.riscos = riscos;
	}
	
}