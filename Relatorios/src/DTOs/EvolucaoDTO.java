package DTOs;

import java.util.ArrayList;
import java.util.List;
/**
 * Classe para retornar dados para inser��o do arquivo
 * @author Gustavo Preto Benvenutti
 * @param consultaAPI - dados para serem processados.
 */
public class EvolucaoDTO {
	private List<org.bson.Document> consultaAPI;
	
	public EvolucaoDTO(List<org.bson.Document> resultadosConsultaAPI) {
		super();
		this.consultaAPI = resultadosConsultaAPI;
	}

	public List<String> getItem1() throws Exception {
		List<String> listItem1 = new ArrayList<>();
		for (org.bson.Document json : this.consultaAPI) {
			listItem1.add((String) json.get("materia"));
		}
		return listItem1;
	}

	public List<String> getItem2() throws Exception {
		List<String> listItem2 = new ArrayList<>();
		for (org.bson.Document json : this.consultaAPI) {
			listItem2.add((String) json.get("nota"));
		}
		return listItem2;
	}

	public List<String> getItem3() throws Exception {
		List<String> listItem3 = new ArrayList<>();
		for (org.bson.Document json : this.consultaAPI) {
			listItem3.add((String) json.get("dia"));
		}
		return listItem3;
	}
	
	public String getNome() {
		return "Gustavo Preto Benvenutti";
	}
	
	public String getDataNasc() {
		return "24/11/2000";
	}
	
	public String getMatricula() {
		return "123456789";
	}
	
	public String getUnidade() {
		return "ApollusEHS";
	}
	
	public String getIdentidade() {
		return "1124136944";
	}
	
	public String getCargo() {
		return "Tester";
	}
	
}
