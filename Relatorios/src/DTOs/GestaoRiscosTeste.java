package DTOs;

import java.util.ArrayList;
import java.util.List;

import classesAuxiliares.Acao;
import classesAuxiliares.Evolucao;
import classesAuxiliares.Funcionario;
import classesAuxiliares.MedidaControle;
import classesAuxiliares.Risco;
import classesAuxiliares.TipoRisco;

public class GestaoRiscosTeste {

	public  GestaoRiscosDTO gerarDTO()  {
		Funcionario funcionario1 = new Funcionario();
		funcionario1.setMatricula("652845");
		funcionario1.setNome("Fernanda Nascimento");
		Funcionario funcionario2 = new Funcionario();
		funcionario2.setMatricula("856482");
		funcionario2.setNome("Renata Andrioli");
		
		List<Risco> riscos = new ArrayList<Risco>();
		Risco risco = new Risco();
		risco.setResultanteRisco("22");
		risco.setCorResultanteRisco("#000000");
		
		
		TipoRisco tipoRisco = new TipoRisco();
		tipoRisco.setDescricaoTipoRisco("Seguran�a");
		tipoRisco.setIdentVar1("Perigo");
		tipoRisco.setIdentVar2("Dano");
		tipoRisco.setMatrizVar1("Probabilidade");
		tipoRisco.setMatrizVar2("Consequ�ncia");
		risco.setTipoRisco(tipoRisco);
		
		risco.setIdentVar1("Confinamento, aprisionamento ou soterramento");
		risco.setIdentVar2(": Contus�es; Ferimentos; �bito");
		risco.setGrupoControle("An�lise de Risco");
		risco.setSubgrupoControle("HAZOP");
		risco.setMoedaPerdaPotencial("USD");
		risco.setValorPerdaPotencial("7600");
		risco.setMemoriaCalculo("30 dias com a AF1 parado, com perda de 216.000t de ferro gusa; Impacto financeiro total de R$ 284.390.598,00\r\n" + 
				"mais custo de reposi��o da fun��o dos equipamentos de R$ 7.760.000,00.");
		
		List<Evolucao> historicoEvolucoes = new ArrayList<Evolucao>();
		Evolucao evolucao1 = new Evolucao();
		evolucao1.setMatrizVar1("Alta");
		evolucao1.setMatrizVar2("Relevante");
		evolucao1.setResultante("22");
		evolucao1.setCorResultante("#B34747");
		evolucao1.setDesejada(false);
		evolucao1.setData("22/09/2019");
		evolucao1.setResponsavel(funcionario1);
		historicoEvolucoes.add(evolucao1);
		
		Evolucao evolucao2 = new Evolucao();
		evolucao2.setMatrizVar1("M�dia");
		evolucao2.setMatrizVar2("Relevante");
		evolucao2.setResultante("8");
		evolucao2.setCorResultante("#92AD47");
		evolucao2.setDesejada(true);
		evolucao2.setData("07/04/2019");
		evolucao2.setResponsavel(funcionario1);
		historicoEvolucoes.add(evolucao2);
		risco.setHistoricoEvolucao(historicoEvolucoes);
		
		List<MedidaControle> medidasControle = new ArrayList<MedidaControle>();
		MedidaControle medida1 = new MedidaControle();
		medida1.setMedidaControle("Barreira f�sica");
		medida1.setMatrizVar1("X");
		medida1.setMatrizVar2("");
		medida1.setData("22/09/2019");
		medida1.setResponsavel(funcionario1);
		medidasControle.add(medida1);
		
		MedidaControle medida2 = new MedidaControle();
		medida2.setMedidaControle("Procedimento");
		medida2.setMatrizVar1("X");
		medida2.setMatrizVar2("X");
		medida2.setData("07/04/2019");
		medida2.setResponsavel(funcionario1);
		medidasControle.add(medida2);
		risco.setMedidasControle(medidasControle);
		
		List<Acao> acoes = new ArrayList<Acao>();
		Acao acao1Risco1 = new Acao();
		acao1Risco1.setCodigo("000512");
		acao1Risco1.setStatus("Pendente");
		acao1Risco1.setPrioridade("Alta");
		acao1Risco1.setPrevisao("09/05/2020");
		acao1Risco1.setDescricao("Avalia��o da estrutura de concreto por especialista em chamin�, membro do comit� mundial.");
		acao1Risco1.setResponsavel(funcionario2);
		acao1Risco1.setTipo("An�lise");
		acao1Risco1.setClassificacao("An�lise");
		acao1Risco1.setAcaoRecorrente("");
		acao1Risco1.setMoedaCustoPrevisto("R$");
		acao1Risco1.setValorCustoPrevisto("2250.00");
		acao1Risco1.setOrigemRecurso("Engenharia");
		acao1Risco1.setAreaExecucao("-");
		acao1Risco1.setDescricaoImplantacao("-");
		acao1Risco1.setMoedaCustoReal("R$");
		acao1Risco1.setValorCustoReal("123456789");
		acao1Risco1.setDataImplantacao("-");
		acoes.add(acao1Risco1);
		
		Acao acao2Risco1 = new Acao();
		acao2Risco1.setCodigo("000513");
		acao2Risco1.setStatus("Vencida");
		acao2Risco1.setPrioridade("Alta");
		acao2Risco1.setPrevisao("17/01/2020");
		acao2Risco1.setDescricao("Avalia��o da estrutura de concreto por especialista em chamin�, membro do comit� mundial.");
		acao2Risco1.setResponsavel(funcionario2);
		acao2Risco1.setTipo("An�lise");
		acao2Risco1.setClassificacao("An�lise");
		acao2Risco1.setAcaoRecorrente("");
		acao2Risco1.setMoedaCustoPrevisto("R$");
		acao2Risco1.setValorCustoPrevisto("2250.00");
		acao2Risco1.setOrigemRecurso("Engenharia");
		acao2Risco1.setAreaExecucao("-");
		acao2Risco1.setDescricaoImplantacao("-");
		acao2Risco1.setMoedaCustoReal("R$");
		acao2Risco1.setValorCustoReal("123456");
		acao2Risco1.setDataImplantacao("-");
		acoes.add(acao2Risco1);
		//risco.setAcoesRisco(acoes);
		riscos.add(risco);
		
		/////////////////////////////////////////////////////////////////////////////////////////
		Risco risco2 = new Risco();
		risco2.setResultanteRisco("18");
		risco2.setCorResultanteRisco("#0dba07");
		
		risco2.setTipoRisco(tipoRisco);
		
		risco2.setIdentVar1("Confinamento, aprisionamento ou soterramento");
		risco2.setIdentVar2("Contus�es; Ferimentos; �bito");
		risco2.setGrupoControle("An�lise de Risco");
		risco2.setSubgrupoControle("HAZOP");
		risco2.setMoedaPerdaPotencial("USD");
		risco2.setValorPerdaPotencial("123456");
		risco2.setMemoriaCalculo("30 dias com a AF1 parado, com perda de 216.000t de ferro gusa; Impacto financeiro total de R$ 284.390.598,00\r\n" + 
				"mais custo de reposi��o da fun��o dos equipamentos de R$ 7.760.000,00.");
		
		List<Evolucao> historicoEvolucoes2 = new ArrayList<Evolucao>();	
		Evolucao evolucao3 = new Evolucao();
		evolucao3.setMatrizVar1("Relevante");
		evolucao3.setMatrizVar2("Relevante");
		evolucao3.setResultante("18");
		evolucao3.setCorResultante("#0dba07");
		evolucao3.setDesejada(false);
		evolucao3.setData("22/09/2019");
		evolucao3.setResponsavel(funcionario1);
		historicoEvolucoes2.add(evolucao3);
		Evolucao evolucao4 = new Evolucao();
		evolucao4.setMatrizVar1("M�dia");
		evolucao4.setMatrizVar2("Relevante");
		evolucao4.setResultante("12");
		evolucao4.setCorResultante("#e6d28e");
		evolucao4.setDesejada(true);
		evolucao4.setData("07/04/2019");
		evolucao4.setResponsavel(funcionario1);
		historicoEvolucoes2.add(evolucao4);
		Evolucao evolucao5 = new Evolucao();
		evolucao5.setMatrizVar1("M�dia");
		evolucao5.setMatrizVar2("Relevante");
		evolucao5.setResultante("8");
		evolucao5.setCorResultante("#ffc500");
		evolucao5.setDesejada(true);
		evolucao5.setData("12/12/2019");
		evolucao5.setResponsavel(funcionario1);
		historicoEvolucoes2.add(evolucao5);
		risco2.setHistoricoEvolucao(historicoEvolucoes2);
		
		
		List<MedidaControle> medidasControle2 = new ArrayList<MedidaControle>();
		MedidaControle medida3 = new MedidaControle();
		medida3.setMedidaControle("Barreira f�sica");
		medida3.setMatrizVar1("");
		medida3.setMatrizVar2("");
		medida3.setData("22/09/2019");
		medida3.setResponsavel(funcionario1);
		medidasControle2.add(medida3);
		
		MedidaControle medida4 = new MedidaControle();
		medida4.setMedidaControle("Procedimento");
		medida4.setMatrizVar1("X");
		medida4.setMatrizVar2("X");
		medida4.setData("07/04/2019");
		medida4.setResponsavel(funcionario1);
		medidasControle2.add(medida4);
		
		risco2.setMedidasControle(medidasControle2);
		
		
		
		List<Acao> acoes2 = new ArrayList<Acao>();
		Acao acao1Risco2 = new Acao();
		acao1Risco2.setCodigo("000514");
		acao1Risco2.setStatus("Pendente");
		acao1Risco2.setPrioridade("Alta");
		acao1Risco2.setPrevisao("09/05/2020");
		acao1Risco2.setDescricao("Avalia��o da estrutura de concreto por especialista em chamin�, membro do comit� mundial.");
		acao1Risco2.setResponsavel(funcionario2);
		acao1Risco2.setTipo("An�lise");
		acao1Risco2.setClassificacao("An�lise");
		acao1Risco2.setAcaoRecorrente("");
		acao1Risco2.setMoedaCustoPrevisto("R$");
		acao1Risco2.setValorCustoPrevisto("22398798.00");
		acao1Risco2.setOrigemRecurso("Engenharia Engenharia Engenharia Engenharia Engenharia Engenharia ");
		acao1Risco2.setAreaExecucao("-");
		acao1Risco2.setDescricaoImplantacao("-");
		acao1Risco2.setMoedaCustoReal("-");
		acao1Risco2.setValorCustoReal("687651");
		acao1Risco2.setDataImplantacao("09/05/2020");
		acoes2.add(acao1Risco2);
		
		Acao acao2Risco2 = new Acao();
		acao2Risco2.setCodigo("000515");
		acao2Risco2.setStatus("Vencida");
		acao2Risco2.setPrioridade("Alta");
		acao2Risco2.setPrevisao("17/01/2020");
		acao2Risco2.setDescricao("Avalia��o da estrutura de concreto por especialista em chamin�, membro do comit� mundial.");
		acao2Risco2.setResponsavel(funcionario2);
		acao2Risco2.setTipo("An�lise");
		acao2Risco2.setClassificacao("An�lise");
		acao2Risco2.setAcaoRecorrente("");
		acao2Risco2.setMoedaCustoPrevisto("R$");
		acao2Risco2.setValorCustoPrevisto("150,00");
		acao2Risco2.setOrigemRecurso("Engenharia");
		acao2Risco2.setAreaExecucao("Avalia��o da estrutura de concreto por especialista em chamin�, membro do comit� mundial.");
		acao2Risco2.setDescricaoImplantacao("-");
		acao2Risco2.setMoedaCustoReal("-");
		acao2Risco2.setValorCustoReal("86746513");
		acao2Risco2.setDataImplantacao("-");
		
		
		acoes2.add(acao2Risco2);
		risco2.setAcoesRisco(acoes2);
		riscos.add(risco2);
		riscos.add(risco2);
		riscos.add(risco2);
		
		
		///////////////////////////////////////////////////////////////////////////////
		Funcionario funcionario3 = new Funcionario();
		funcionario3.setMatricula("542159");
		funcionario3.setNome("Jo�o Antunes");
		Funcionario funcionario4 = new Funcionario();
		funcionario4.setMatricula("23115");
		funcionario4.setNome("Carla Garcia");
		
		
		GestaoRiscosDTO dto = new GestaoRiscosDTO();
		dto.setCodigoCenario("0025180");
		
		dto.setProcessoVisivel(true);
		dto.setConfiguracaoAprovacao("NAO_NECESSITA_DE_APROVACAO");
		dto.setConfiguracaoAssociacaoCenarios("NAO_NECESSITA_ASSOCIACAO");
		dto.setClassificacaoVisivel(false);
		dto.setOrigemRecursoVisivel(true);
		dto.setAreaExecucaoVisivel(false);
		
		dto.setCenario("Desabamento do topo ou partes da estrutura da chamin� dos regeneradores do Alto Forno 1");
		dto.setStatus("Aprovado");
		dto.setDataCadastro("04/04/2019");
		dto.setProcesso("Produ��o");
		dto.setH1("Regi�o SP");
		dto.setH2("Produ��o");
		dto.setH3("-");
		List<String> locais = new ArrayList<String>();
		locais.add("1411AC2 - Carregador de Baterias");
		locais.add("21321AF - Alto Forno");
		dto.setLocais(locais);
		dto.setDetalhamentoCenario("Desabamento do topo ou plataformas da chamin� de concreto dos regeneradores do Alto Forno 1, em fun��o dos\r\n" + 
				"danos da explos�o ocorrida em 19/04/2010 no canal de fuma�a dos regeneradores do AF1.");
		
		dto.setResponsavel(funcionario3);
		dto.setAprovador(funcionario4);
		dto.setRevisao("06");
		dto.setAprovadoEm("");
		dto.setRevisadoEm("");
		dto.setRegistroRestrito("x");
		//List<String> cenariosAssociados = new ArrayList<String>();
		//cenariosAssociados.add("225485 (Intermedi�rio) - Comprometimento do isolamento t�rmico do forno do isolamento t�rmico do forno");
		//cenariosAssociados.add("252545 (Intermedi�rio) - Desabamento do topo ou partes da estrutura do isolamento t�rmico do forno");
		//dto.setCenariosAssociados(cenariosAssociados);
		dto.setRiscos(riscos);
		
		return dto;
	}
}
