package Utilitarios;
import java.awt.Color;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.aspose.words.Cell;
import com.aspose.words.DocumentBuilder;
import com.aspose.words.LineStyle;
import com.aspose.words.ParagraphAlignment;
import com.aspose.words.PreferredWidth;
import com.aspose.words.Table;

/** 
 * Classe com m�todos utiit�rios, a fim de auxiliar e reaproveitar os m�todos j� criados
 * @author Gustavo Preto Benvenutti
 */
public class Utils {

	/** 
	 * M�todo para redirecionar para determinado diret�rio
	 * @param diretorio - primeiro diret�rio a ser acessado.
	 * @param diretorio2 - segundo diret�rio a ser acessado (este estando "dentro" do diret�rio1).
	 * @return "caminho" para diret�rios informados anteriormente, formato Stringss
	 */
	public static String getDataDir(String diretorio1, String diretorio2) {
        File dir = new File(System.getProperty("user.dir")); //DIRET�RIO ATUAL DE TRABALHO
        dir = new File(dir, diretorio1);
        dir = new File(dir, diretorio2);       
        return dir.toString() + File.separator;
    }
	
	/** 
	 * M�todo para formatar datas.
	 * @param formatacao - formato de data desejado (Ex.: dd/MM/yyyy).
	 * @param data - data a ser formatada.
	 * @return data com formata��o requerido, formato String.
	 */
	public static String formatarData(String formatacao, Date data) {
		DateFormat dateFormat = new SimpleDateFormat(formatacao);
		String dataFormatada = dateFormat.format(data);
		return dataFormatada;
	}
	
	/** 
	 * M�todo para formatar tabelas.
	 * @param tabela - Table, tabela a ser formatada
	 * @param preferredWidth - largura preferida da table.
	 * @param leftIndent - identa��o � esquerda.
	 */
	public static void formatarTabela(Table tabela, double preferredWidth, int leftIndent) throws Exception {
		tabela.setPreferredWidth(PreferredWidth.fromPercent(preferredWidth));
		tabela.setLeftIndent(leftIndent);
	}
	
	/** 
	 * M�todo para formatar c�lula.
	 * @param builder - builder utilizado.
	 * @param celula - Table, tabela a ser formatada
	 * @param preferredWidth - largura preferida da table.
	 * @param leftPadding - padding � esquerda.
	 * @param rightPadding - padding � direita.
	 * @param topPadding - padding superior.
	 * @param bottomPadding - padding inferior.
	 * @param backgroundColor - cor de fundo da c�lula.
	 * @param leftIndent - identa��o � esquerda.
	 */
	public static void formatarCelula(DocumentBuilder builder, double preferredWidth, int leftPadding, int rightPadding, int topPadding, int bottomPadding, Color backgroundColor) throws Exception {
		builder.getCellFormat().setPreferredWidth(PreferredWidth.fromPercent(preferredWidth));
		builder.getCellFormat().setLeftPadding(leftPadding);
		builder.getCellFormat().setRightPadding(rightPadding);
		builder.getCellFormat().setTopPadding(topPadding);
		builder.getCellFormat().setBottomPadding(bottomPadding);
		builder.getCellFormat().getShading().setBackgroundPatternColor(backgroundColor);
	}
	
	/** 
	 * M�todo para formatar par�grafo.
	 * @param builder - builder utilizado.
	 * @param alignment -
	 * 		0 - Left;
	 * 		1 - Center;
	 * 		2 - Right;
	 * 		3 - Justify;
	 * 		4 - Distributed;
	 * @param lineSpacing - espa�amento entre linhas.
	 */
	public static void formatarParagrafo(DocumentBuilder builder, int alignment, double lineSpacing) {
		builder.getParagraphFormat().setAlignment(alignment);
		builder.getParagraphFormat().setLineSpacing(lineSpacing);
	}
	
	public static void configurarFonte(DocumentBuilder builder, String fonte, double fontSize, boolean bold) {
		com.aspose.words.Font font = builder.getFont();
		font.setName(fonte);
		font.setSize(fontSize);
		builder.setBold(bold);
	}
	
}