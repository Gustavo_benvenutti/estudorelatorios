package Utilitarios;
import com.mongodb.DB;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.ServerAddress;
import com.mongodb.MongoCredential;
import com.mongodb.MongoClientOptions;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.*;
import com.mongodb.client.model.CreateCollectionOptions;
import com.mongodb.client.model.ValidationOptions;
import com.mongodb.*;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Filters;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Projections.*;
import com.mongodb.client.model.Sorts;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;

/** 
 * Classe criada para conex�o com mongoDB e obter dados para serem exibidos. Utilizada somente nos estudos para finalidade 
  	descrita anteriormente.
 * @return list<Document> com dados dos cadastros
 * @throws Exception
 */
public class ConsultaAPI {
	
	MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb+srv://PretoBranco:PretoBranco@cluster0-ih2tx.mongodb.net/test?retryWrites=true&w=majority"));
	MongoDatabase database = mongoClient.getDatabase("CriandoAPI");
	
	public List<Document > executar() throws Exception  {
		List<Document > resultadosConsultaAPI = new ArrayList<>();
		
		//MongoCollection<org.bson.Document> coll = database.getCollection("data");
		MongoCollection<org.bson.Document> coll = database.getCollection("GestaoRiscos");
		
		//coll.deleteMany(eq("dia", "2020-03-10"));
		
		/*
		Document doc = new Document("cenario", " 0025180 - Desabamento do topo ou partes da estrutura da chamin� dos regeneradores do Alto Forno 1 ")
				.append("status", "Aprovado")
				.append("dataCadastro", "04/04/2019")
				.append("processo", "Produ��o")
				.append("unidade", "Regi�o SP")
				.append("area", "Produ��o")
				.append("celula", "-")
				.append("locais", "1411AC2 - Carregador de Baterias")
				.append("locais", "21321AF - Alto Forno")
				.append("detalhamentoCenario", "Desabamento do topo ou plataformas da chamin� de concreto dos regeneradores do Alto Forno 1, em fun��o dos danos da explos�o ocorrida em 19/04/2010 no canal de fuma�a dos regeneradores do AF1.")
				.append("responsavel", "542159 � Jo�o Antunes")
				.append("aprovador", "23115 � Carla Garcia")
				.append("revisao", "06")
				.append("aprovadoEm", "")
				.append("RevisadoEm", "")
				.append("registroRestrito", "1")
				.append("cenariosAssociados", "225485 (Intermedi�rio) - Comprometimento do isolamento t�rmico do forno do isolamento t�rmico do forno")
				.append("cenariosAssociados", "252545 (Intermedi�rio) - Desabamento do topo ou partes da estrutura do isolamento t�rmico do forno");
		coll.insertOne(doc);
		*/
		
		/*
		for (int i = 2; i < 60; i++) {
			Document doc2 = new Document("materia", "MongoDB"+i)
		            .append("nota", ""+i)
		           .append("dia", "2020-03-10");
			coll.insertOne(doc2);
		}
		*/
		
		MongoCursor<Document> cursor = coll.find().iterator();
		try {
		    while (cursor.hasNext()) {
		    	Object obj = cursor.next().toJson();
		    	Document doc1 = Document.parse((String) obj);
		    	resultadosConsultaAPI.add(doc1);
		    }
		} finally {
		    cursor.close();
		}
		return resultadosConsultaAPI;
		
	}
	
	public List<Document> consultaLocais(){
		List<Document > resultadosConsultaLocais = new ArrayList<>();
		MongoCollection<org.bson.Document> collectionLocais = database.getCollection("locais");
		//MongoCollection<org.bson.Document> collectionLocais = database.getCollection("testeVazios");
		MongoCursor<Document> cursor3 = collectionLocais.find().iterator();
		try {
		    while (cursor3.hasNext()) {
		    	Object obj = cursor3.next().toJson();
		    	Document doc1 = Document.parse((String) obj);
		    	resultadosConsultaLocais.add(doc1);
		    }
		} finally {
			cursor3.close();
		}
		return resultadosConsultaLocais;
	}
	
	public List<Document > consultaCenariosAssociados(){
		List<Document > resultadosConsultaCenariosAssociados = new ArrayList<>();
		MongoCollection<org.bson.Document> collectionGestRiscos = database.getCollection("cenariosAssociados");
		//MongoCollection<org.bson.Document> collectionGestRiscos = database.getCollection("testeVazios");
		MongoCursor<Document> cursor2 = collectionGestRiscos.find().iterator();
		try {
		    while (cursor2.hasNext()) {
		    	Object obj = cursor2.next().toJson();
		    	Document doc1 = Document.parse((String) obj);
		    	resultadosConsultaCenariosAssociados.add(doc1);
		    }
		} finally {
			cursor2.close();
		}
		
		return resultadosConsultaCenariosAssociados;
	}
}
